/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiiexamenjoseandresmirandaariaspi;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Luis David Chavarria Cubero
 */
public class Persona {

    private int x;
    private int y;
    private int dir;
    private int num;
    private int posx;
    private int posy;
    private int vidas;

    public Persona(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Persona() {
        posx = 0;
        posy = 0;
        vidas = 3;
    }
    /***
     * Metodo para pintar el personaje quieto sin realizar ningun movimiento
     * @param g Variable para poder pintar
     */
    public void pintarNormal(Graphics g) {
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 250 + (posx * 20), y + 250 + (posy * 20), 60, 60);
        g.setColor(new Color(11, 189, 204));
        g.fillRect(x + 250 + (posx * 20), y + 300 + (posy * 20), 60, 110);
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 260 + (posx * 20), y + 310 + (posy * 20), 15, 80);
        g.setColor(new Color(36, 18, 196));
        g.fillRect(x + 270 + (posx * 20), y + 410 + (posy * 20), 20, 80);
        g.fillRect(x + 270 + (posx * 20), y + 410 + (posy * 20), 20, 80);

    }
    /***
     * Metodo para dibujar al personaje mientras esta saltando
     * @param g Variable para poder pintar
     */
    public void pintarSaltando(Graphics g) {
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 250 + (posx * 20), y + 230 + (posy * 20), 60, 60);
        g.setColor(new Color(36, 18, 196));
        int[] xs1 = {71 + 1205 + (posx * 20), 57 + 1205 + (posx * 20), 20 + 1205 + (posx * 20), 34 + 1205 + (posx * 20)};
        int[] ys1 = {114 + 362 + (posy * 20), 114 + 362 + (posy * 20), 170 + 362 + (posy * 20), 170 + 362 + (posy * 20)};
        int[] xs2 = {10 + 1265 + (posx * 20), 24 + 1265 + (posx * 20), 71 + 1265 + (posx * 20), 57 + 1265 + (posx * 20)};
        int[] ys2 = {114 + 362 + (posy * 20), 114 + 362 + (posy * 20), 170 + 362 + (posy * 20), 170 + 362 + (posy * 20)};
        g.fillPolygon(xs1, ys1, xs1.length);
        g.fillPolygon(xs2, ys2, xs2.length);
        g.setColor(new Color(11, 189, 204));
        g.fillRect(x + 250 + (posx * 20), y + 280 + (posy * 20), 60, 110);
        g.setColor(new Color(211, 112, 72));
        int[] xs = {10 + 1200 + (posx * 20), 24 + 1200 + (posx * 20), 71 + 1200 + (posx * 20), 57 + 1200 + (posx * 20)};
        int[] ys = {114 + 230 + (posy * 20), 114 + 230 + (posy * 20), 165 + 230 + (posy * 20), 165 + 230 + (posy * 20)};
        g.fillPolygon(xs, ys, xs.length);

    }

    
    /***
     * Metodo para pintar al personaje mientras camina
     * @param g Variable para poder pintar
     * @param dir la direccion en la que va el personaje
     */
    public void pintarCamina(Graphics g, int dir) {
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 250 + (posx * 20), y + 250 + (posy * 20), 60, 60);
        g.setColor(new Color(36, 18, 196));

        if (dir == 1) {

            int[] xs2 = {3 + 1280 + (posx * 20), 25 + 1280 + (posx * 20), 51 + 1280 + (posx * 20), 37 + 1280 + (posx * 20)};
            int[] ys2 = {107 + 382 + (posy * 20), 107 + 382 + (posy * 20), 200 + 382 + (posy * 20), 200 + 382 + (posy * 20)};
            int[] xs1 = {37 + 1220 + (posx * 20), 51 + 1220 + (posx * 20), 25 + 1220 + (posx * 20), 3 + 1220 + (posx * 20)};
            int[] ys1 = {107 + 382 + (posy * 20), 107 + 382 + (posy * 20), 200 + 382 + (posy * 20), 200 + 382 + (posy * 20)};
            g.fillPolygon(xs1, ys1, xs1.length);

            g.fillPolygon(xs2, ys2, xs2.length);

        } else if (dir == -1) {

            int[] xs2 = {3 + 1280 + (posx * 20), 25 + 1280 + (posx * 20), 51 + 1280 + (posx * 20), 37 + 1280 + (posx * 20)};
            int[] ys2 = {107 + 382 + (posy * 20), 107 + 382 + (posy * 20), 200 + 382 + (posy * 20), 200 + 382 + (posy * 20)};
            int[] xs1 = {37 + 1220 + (posx * 20), 51 + 1220 + (posx * 20), 25 + 1220 + (posx * 20), 3 + 1220 + (posx * 20)};
            int[] ys1 = {107 + 382 + (posy * 20), 107 + 382 + (posy * 20), 200 + 382 + (posy * 20), 200 + 382 + (posy * 20)};
            g.fillPolygon(xs1, ys1, xs1.length);
            g.fillPolygon(xs2, ys2, xs2.length);

        } else {
            g.fillRect(x + 270 + (posx * 20), y + 410 + (posy * 20), 20, 80);
            g.fillRect(x + 270 + (posx * 20), y + 410 + (posy * 20), 20, 80);
        }

        g.setColor(new Color(11, 189, 204));
        g.fillRect(x + 250 + (posx * 20), y + 300 + (posy * 20), 60, 110);
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 260 + (posx * 20), y + 310 + (posy * 20), 15, 80);
    }

    
    /***
     * Metodo para pintar al personaje cuando esta agachado
     * @param g Variable para poder pintar
     */
    public void pintarAgachado(Graphics g) {

        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 250 + (posx * 20), y + 280 + (posy * 20), 60, 50);
        g.setColor(new Color(11, 189, 204));
        g.fillRect(x + 250 + (posx * 20), y + 330 + (posy * 20), 60, 110);
        g.setColor(new Color(211, 112, 72));
        g.fillRect(x + 260 + (posx * 20), y + 340 + (posy * 20), 15, 80);
        g.setColor(new Color(36, 18, 196));
        g.fillRect(x + 270 + (posx * 20), y + 440 + (posy * 20), 20, 60);
        g.fillRect(x + 270 + (posx * 20), y + 440 + (posy * 20), 20, 60);

    }
    /***
     * Metodo para formar los bounds del personaje
     * @param numParte El numero del bound donde choco
     * @return 
     */
    public Rectangle getBounds(int numParte) {

        if (numParte == 1) {
            return new Rectangle(x + 250 + (posx * 20), y + 250 + (posy * 20), 60, 60);
        } else {
            return new Rectangle(x + 270 + (posx * 20), y + 410 + (posy * 20), 20, 110);
        }
    }
    /***
     * Metodo para mostrar cuantas vidas tiene
     * @param g Variable para poder dibujar
     * @param v La cantidad de vidas que tiene
     */
    public void pintarMostrarVidas(Graphics g, int v) {
        Font f;
        g.setColor(Color.BLACK);
        f = new Font("DUMMY", Font.BOLD, 25);
        g.setFont(f);
        g.drawString("Vidas Restantes: "+ v,10, 30);
    }

    
    /***
     * Metodo para mostrar cuantas monedas tiene
     * @param g Variable para poder dibujar
     * @param m La cantidad de monedas
     */
    public void pintarMostrarMonedas(Graphics g, int m) {
        Font f;
        g.setColor(Color.BLACK);
        f = new Font("DUMMY", Font.BOLD, 25);
        g.setFont(f);
        g.drawString("Monedas totales: " + m, 1110, 30);
    }

    
     
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public void cambiarPosx(int dir) {
        posx += dir;
    }

    public void cambiarPosy(int dir) {
        posy += dir;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    
    
    
    
}
