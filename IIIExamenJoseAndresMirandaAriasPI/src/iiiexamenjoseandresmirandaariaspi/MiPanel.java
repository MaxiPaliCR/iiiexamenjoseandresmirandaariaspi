/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiiexamenjoseandresmirandaariaspi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private int cX;
    private int cY;
    private Persona persona;
    private Jugar juego;
    private boolean salta;
    private boolean camina;
    private boolean agacharse;
    private boolean normal;
    private int dir;

    public MiPanel() {
        setBackground(Color.WHITE);
        setFocusable(true);
        addKeyListener(this);
        persona = new Persona(1000, 100);
        juego = new Jugar(1);
        normal = true;
        agacharse = false;
        camina = true;
        salta = false;

    }

    public int numeroRandom(){
        int RAM = (int)(Math.random()*4+1);
        return RAM;
    }
    
    
    
    
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1400, 650);
    }

    
    

    
    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        
   
        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;
        juego.setX(juego.getX()+ 3);
        juego.tipo(g);
        if (juego.getX() >= getWidth()) {
            juego.setX(-200);
            juego.setTipo(numeroRandom());
            
        }
        
     
        persona.pintarMostrarMonedas(g2, 2);
        persona.pintarMostrarVidas(g2, 3);
        if (normal) {
            persona.pintarNormal(g);
        } else if (camina) {
            persona.pintarCamina(g, dir);
        } else if (salta && !agacharse) {
            persona.pintarSaltando(g);
        } else if (agacharse && !salta){
            persona.pintarAgachado(g);
        }
        
        g.setColor(new Color(131, 81, 54));
        g2.fillRect(0, 579, 1400, 100);
        g.setColor(new Color(61, 146, 39));
        g2.fillRect(0, 579, 1400, 20);
        
        
        //   g.drawLine(0, cY, getWidth(), cY);
        //    g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke
    ) {

    }

    @Override
    public void keyPressed(KeyEvent ke
    ) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            camina = true;
            persona.cambiarPosx(-1);
            normal = false;
            dir = -1;

        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            camina = true;
            persona.cambiarPosx(1);
            normal = false;
            dir = 1;

        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            agacharse = true;
            salta = false;
            camina = false;
            normal = false;
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            salta = true;
            agacharse = false;
            camina = false;
            normal = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke
    ) {

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            normal = true;
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {;
            normal = true;
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            normal = true;
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            normal = true;
        }
    }

 
}
