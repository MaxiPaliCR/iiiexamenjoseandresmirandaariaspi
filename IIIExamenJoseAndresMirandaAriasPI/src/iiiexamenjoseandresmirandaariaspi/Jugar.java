/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiiexamenjoseandresmirandaariaspi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Luis David Chavarria Cubero
 */
public class Jugar {

    private int posx;
    private int posy;
    private int x;
    private int tipo;

    public Jugar(int tipo) {
        this.tipo = tipo;
    }
    /***
     * Metodo que dibuja el obstaculo numero 1
     * @param g Variable para poder pintar
     */
    public void pintarObjetoUno(Graphics g) {
        g.setColor(Color.DARK_GRAY);
        g.fillOval(x, 534, 43, 43);
   
    }
    /*** 
     * Metodo que dibuja el obstaculo numero 2
     * @param g Variable para poder pintar
     */
    public void pintarObjetoDos(Graphics g) {
        g.fillRect(x, 320, 40, 40);
          
    }
    /***
     * Metodo que dibula el obstaculo numero 3
     * @param g Variable para poder pintar
     */
    public void pintarObjetoTres(Graphics g) {
       g.fillOval(x, 300, 50, 40);
       
    }
    /***
     * Variable que dibuja las monedas
     * @param g Variable para poder pintar
     */
    public void pintarMoneda(Graphics g) {
        g.setColor(new Color(252, 201, 27));
        g.fillOval(x + 100, 300, 50, 50);
   
    }
    /***
     * Metodo que dibuja cada uno de los obstaculos uno por uno
     * @param g Variable para poder pintar
     */
    public void tipo(Graphics g) {
        if (tipo == 1) {
            pintarObjetoUno(g);
        } else if (tipo == 2) {
            pintarMoneda(g);
        } else if (tipo == 3) {
            pintarObjetoDos(g);
        } else if (tipo == 4) {
            pintarObjetoTres(g);
        }
    }

    /***
     * Metodo que dibuja los bounds de los obstaculos
     * @param numObjeto Recibe el numero del obstaculo
     * @return Devuelve el obstaculo dibujado
     */
    public Rectangle getBounds(int numObjeto){
        if (numObjeto == 1){
        return new Rectangle(x+2, 536, 38, 38);
        } else if (numObjeto == 2){
            return new Rectangle(x, 320, 40, 40);
            
        } else if (numObjeto == 3){
            return new Rectangle(x + 6, 300 + 4, 35, 35);
            
        }
        else{
            return new Rectangle(x + 100 + 5, 300 + 5, 40, 40);
        }
    }
    
   
     

    
    
    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

}
